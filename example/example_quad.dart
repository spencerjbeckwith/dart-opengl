import 'dart:ffi';
import 'dart:math';
import 'dart:typed_data';

import 'package:glfw/glfw.dart';
import 'package:opengl/opengl.dart';
import 'package:ffi/ffi.dart';
import 'package:ffi_utils/ffi_utils.dart';

const int width = 640;
const int height = 480;
late Pointer<Uint32> arrays;
late Pointer<Uint32> textures;
late int program;
late int locationTime;
late int locationTexture;

void main() {
  loadGl();
  glfwInit();
  print('GLFW: ${glfwGetVersionString().cast<Utf8>().toDartString()}');

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  final window = glfwCreateWindow(width, height,
      'Dart FFI + GLFW + OpenGL'.toNativeUtf8(), nullptr, nullptr);
  glfwMakeContextCurrent(window);

  print('GL_VENDOR: ${glGetString(GL_VENDOR).cast<Utf8>().toDartString()}');
  print('GL_RENDERER: ${glGetString(GL_RENDERER).cast<Utf8>().toDartString()}');
  print('GL_VERSION: ${glGetString(GL_VERSION).cast<Utf8>().toDartString()}');

  prepare(width, height);

  while (glfwWindowShouldClose(window) != GLFW_TRUE) {
    draw(DateTime.now().millisecondsSinceEpoch);

    glfwSwapBuffers(window);
    glfwPollEvents();
  }
}

void draw(int timeMs) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(program);
  final phase = (timeMs % 1000) / 1000.0 * 2 * pi;
  glUniform1f(locationTime, sin(phase) * 0.2 + 0.8);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, textures[0]);
  glUniform1i(locationTexture, 0);

  glBindVertexArray(arrays[0]);
  glDrawArrays(GL_TRIANGLES, 0, 6);

  glBindVertexArray(0);
}

void prepare(int width, int height) {
  glClearColor(0.0, 0.7, 0.99, 0.0);
  glViewport(0, 0, width, height);

  final vertexShader = glCreateShader(GL_VERTEX_SHADER);
  compileShader(vertexShader, vertexCode);

  final fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  compileShader(fragmentShader, fragmentCode);

  program = glCreateProgram();
  glAttachShader(program, vertexShader);
  glAttachShader(program, fragmentShader);

  glBindAttribLocation(program, 0, NativeString.fromString('in_Position'));
  glBindAttribLocation(program, 1, NativeString.fromString('in_Color'));
  glBindAttribLocation(program, 2, NativeString.fromString('in_TextCoords'));

  glLinkProgram(program);

  locationTime =
      glGetUniformLocation(program, NativeString.fromString('in_Time'));
  locationTexture =
      glGetUniformLocation(program, NativeString.fromString('texture0'));

  arrays = calloc<Uint32>();
  glGenVertexArrays(1, arrays);

  final buffers = calloc<Uint32>(3);
  glGenBuffers(3, buffers);

  glBindVertexArray(arrays[0]);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
  glBufferData(GL_ARRAY_BUFFER, positionData.lengthInBytes,
      NativeBuffer.fromTyped(positionData), GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
  glBufferData(GL_ARRAY_BUFFER, colorData.lengthInBytes,
      NativeBuffer.fromTyped(colorData), GL_STATIC_DRAW);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(1);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
  glBufferData(GL_ARRAY_BUFFER, uvData.lengthInBytes,
      NativeBuffer.fromTyped(uvData), GL_STATIC_DRAW);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(2);

  textures = calloc<Uint32>();
  glGenTextures(1, textures);

  glBindTexture(GL_TEXTURE_2D, textures[0]);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE,
      NativeBuffer.fromTyped(textureData));
}

void compileShader(int shader, String text) {
  glShaderSource(shader, 1, NativeStringArray.fromList([text]), nullptr);
  glCompileShader(shader);

  final status = calloc<Int32>();
  glGetShaderiv(shader, GL_COMPILE_STATUS, status);
  if (status.value != GL_TRUE) {
    print('shader status ${status.value}');

    final infoLength = calloc<Int32>();
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, infoLength);

    final length = calloc<Int32>();
    final infoLog = NativeString.fromLength(infoLength.value + 1);
    glGetShaderInfoLog(shader, infoLength.value, length, infoLog);

    if (length.value > 0) {
      print(infoLog.ref);
    }
  }
}

const vertexCode = '''
#version 330

uniform float in_Time;
in vec3 in_Position;
in vec3 in_Color;
in vec2 in_TextCoords;
out vec3 ex_Color;
out vec2 ex_TextCoords;

void main(void)
{
  gl_Position = vec4(in_Position.x*in_Time, in_Position.y*in_Time, in_Position.z*in_Time, 1.0);
  ex_Color = vec3(in_Color.r, in_Color.g, in_Color.b);
  ex_TextCoords = in_TextCoords;
}
''';

const fragmentCode = '''
#version 330

uniform sampler2D texture0;
in vec3 ex_Color;
in vec2 ex_TextCoords;
out vec4 out_Color;

void main(void)
{
  vec4 tex = texture(texture0, ex_TextCoords).xyzw;
  vec4 color = vec4(ex_Color,1.0);
  out_Color = mix(tex, color, 0.9);
}
''';

var positionData = Float32List.fromList([
  1.0,
  1.0,
  -1.0,
  -1.0,
  -1.0,
  -1.0,
  1.0,
  -1.0,
  -1.0,
  1.0,
  1.0,
  -1.0,
  -1.0,
  -1.0,
  -1.0,
  -1.0,
  1.0,
  -1.0
]);

var colorData = Float32List.fromList([
  1.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  1.0,
  1.0,
  0.0,
  0.0,
  0.0,
  1.0,
  0.0,
  0.0,
  0.0,
  1.0
]);

var uvData = Float32List.fromList(
    [0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0]);

var textureData = Uint8List.fromList([
  255,
  0,
  255,
  255,
  255,
  255,
  255,
  255,
  255,
  255,
  255,
  255,
  255,
  0,
  255,
  128
]);
